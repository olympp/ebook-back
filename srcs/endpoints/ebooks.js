const EBook = require("../models/ebooks")

module.exports = {
  get: [
    async srv => {
      const filters = []
      if (srv.get.skip && !isNaN(Number(srv.get.skip)))
        filters.push(a => a.skip(Number(srv.get.skip)))

      if (srv.get.limit && !isNaN(Number(srv.get.limit)))
        filters.push(a => a.limit(Number(srv.get.limit)))

      const req = {
        filter: a => filters.reduce((acc, val) => val(acc), a),
        format: e => e.format(),
      }
      if (srv.get.match && srv.get.match.trim()) {
        const match = new RegExp(srv.get.match.trim(), "iu")
        req.query = {$or: [
          {desc: {$regex: match}},
          {title: {$regex: match}},
          {"meta.Author": {$regex: match}},
          {"meta.Category": {$regex: match}},
        ]}
      }

      const ebooks = await EBook.getAll(req)
      srv.send({ebooks})
    },
  ],
}
