const {Engine} = require("ormon")
const pkg = require("../../package.json")

module.exports = {
  get: [
    async srv => {
      const version = await Engine.Driver.findOne("_versions", {}, {sort: [["version", -1]]}) // eslint-disable-line no-magic-numbers, max-len
      srv.send({
        db: version.version,
        env: process.env.NODE_ENV, // eslint-disable-line no-process-env
        version: pkg.version,
      })
    },
  ],
}
